'use strict';

/* Controllers */

/*var listApp = angular.module('listApp', []);

phonecatApp.controller('ListCtrl', function($scope) {
  $scope.phones = [
    {'name': 'Nexus S',
     'snippet': 'Fast just got faster with Nexus S.'},
    {'name': 'Motorola XOOM™ with Wi-Fi',
     'snippet': 'The Next, Next Generation tablet.'},
    {'name': 'MOTOROLA XOOM™',
     'snippet': 'The Next, Next Generation tablet.'}
  ];
});*/

(function() {
    "use strict";
    var ListApp = angular.module("ListApp", []);
    var ListController = function($log) {
        var vm = this;
        vm.item = "";
        vm.quantity = 1;
        vm.todolist = [];
        vm.addToCart = function() {
            vm.cart.push({
                item: vm.item,
                quantity: vm.quantity
            });
            $log.info("Added " + vm.quantity + " item of " + vm.item + " to cart");
            vm.item = "";
            vm.quantity = 1;
        };
    };
    ListApp.controller("ListController", ["$log", ListController]);
})();