/**
 * Created by Nazir on 7/4/16.
 */
'use strict';

(function() {
    "use strict";
    var ListApp = angular.module("ListApp", []);
    var ListController = function($log) {
        var vm = this;
        vm.item = "";
        vm.completed = false;
        vm.quantity = 1;
        vm.todoList = [];
        vm.addToList = function() {
            vm.cart.push({
                item: vm.item,
                completed: vm.completed
            });
            $log.info("Added " + vm.item + " to list");
            vm.item = "";
            vm.completed = false;
        };
    };
    ListApp.controller("ListController", ["$log", ListController]);
})();